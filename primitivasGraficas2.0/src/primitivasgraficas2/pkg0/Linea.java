package primitivasgraficas2.pkg0;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;


public class Linea extends Figura {
    Point2D punto1;
    Point2D punto2;
    
    
    Linea(Point _p1, Point _p2, Color _color) {
        punto1 = new Point(_p1.x,_p1.y);
        punto2 = new Point(_p2.x,_p2.y);
        color  = _color;
    }    
}
