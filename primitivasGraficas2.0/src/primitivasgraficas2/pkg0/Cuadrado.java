package primitivasgraficas2.pkg0;
import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;


public class Cuadrado extends Figura{
    Point punto1;
    Point punto2;
    Point punto3;
    Point punto4;
    Cuadrado(Point p1, Point p2,Point p3, Point p4, Color _color){
    punto1=new Point(p1.x,p1.y);
    punto2=new Point(p2.x,p2.y);
    punto3=new Point(p3.x,p3.y);
    punto4=new Point(p4.x,p4.y);
    color=_color;
    }
}
